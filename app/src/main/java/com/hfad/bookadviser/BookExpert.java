package com.hfad.bookadviser;

import java.util.ArrayList;
import java.util.List;

public class BookExpert {
    List<String> getBooks(String genre) {
        List<String> books = new ArrayList<>();
        if(genre.equals("Fiction")) {
            books.add("1984");
            books.add("Fahrenheit 451");
        }else if(genre.equals("Non-Fiction")){
            books.add("Silent Spring");
            books.add("Into Thin Air");
        }else if(genre.equals("Horror")){
            books.add("Frankenstein");
            books.add("Dracula");
        }else if(genre.equals("Drama")){
            books.add("Hamlet");
            books.add("Macbeth");
        }
        return books;
    }
}
