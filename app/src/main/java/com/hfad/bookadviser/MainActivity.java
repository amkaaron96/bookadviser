package com.hfad.bookadviser;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickFindBook(View view) {
        BookExpert expert = new BookExpert();

        TextView books = (TextView) findViewById(R.id.books);

        Spinner genre = (Spinner) findViewById(R.id.genre);

        String bookType = String.valueOf(genre.getSelectedItem());

        List<String> booksList = expert.getBooks(bookType);
        StringBuilder booksFormatted = new StringBuilder();
        for(String book : booksList) {
            booksFormatted.append(book).append('\n');
        }

        books.setText(booksFormatted);
    }

}
